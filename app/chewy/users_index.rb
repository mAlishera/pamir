class UsersIndex < Chewy::Index
  settings analysis: {
    analyzer: {
      email: {
        tokenizer: 'keyword',
        filter: ['lowercase']
      }
    }
  }

  define_type User.includes(:spouse, :roles, :languages, :children) do

    field :first_name, :last_name, :middle_name, :university
    field :email, analyzer: 'email'
    field :spouse, value: ->(user) {
      user.spouse.present?
    }
    field :children, value: ->(user) {
      !user.children.present?
    }
    field :gender, value: ->(user) {
      if user.gender == :female
        "female"
      else
        "male"
      end
    }
    field :nationality, value: ->(user) {
      if user.nationality == :tajikistan
        "tajikistan"
      else
        "russian"
      end
    }
    field :roles, value: ->(user) { user.roles.pluck(:id) }
    field :languages, value: ->(user) { user.languages.pluck(:id) }
  end
end
