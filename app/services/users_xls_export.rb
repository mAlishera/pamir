class UsersXlsExport < UsersCsvExport

  def initialize(users)
    @users = users
    @options = {col_sep: "\t"}
  end
end
