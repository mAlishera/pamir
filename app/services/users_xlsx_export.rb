class UsersXlsxExport

  def initialize(users)
    @users = users
  end

  def perform
    @package = Axlsx::Package.new
    @package.use_shared_strings = true
    workbook = @package.workbook
    workbook.add_worksheet(name: "export") do |sheet|
      sheet.add_row ["first_name", "last_name", "middle_name", "id", "Role 1", "email", "phone"]
      @users.each do |s|
        sheet.add_row [s.first_name, s.last_name, s.middle_name, s.id, s.roles.first.name, s.email, s.phone]
      end
    end
    @package.to_stream.read
  end
end
