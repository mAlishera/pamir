class UsersImport

  class InvalidFileFormat < StandardError
  end

  attr_accessor :file

  def initialize(file)
    @file = file
  end

  def perform
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      user = User.find_by_id(row["id"]) || User.new
      user.attributes = row.to_hash.slice(*accessible_attributes)
      user.phone =  user.phone.to_i
      user.save!
    end
  end

  private

  def accessible_attributes
    %w(first_name last_name middle_name phone birthday email profession gender grade university year_of_grace other_lang
       user_helps_gamoat bp_country bp_address bp_gamoat lp_country lp_address lp_district_metro passport_date_of_issue
       passport_number year_of_marriage year_of_graduation current_occupation income nationality state_of_living drivers_license
       state_of_occupation reservist state_of_separation marital_status international_passport other_nationality license_category)
  end

  def open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv"
      Roo::Csv.new(file.path)
    when ".xls"
      Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx"
      Roo::Excelx.new(file.path, nil, :ignore)
    else
      raise InvalidFileFormat, "Неизвестный тип файла: #{file.original_filename}"
    end
  end
end
