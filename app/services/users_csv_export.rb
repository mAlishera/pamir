class UsersCsvExport
  def initialize(users)
    @users = users
    @options = {}
  end

  def perform
    CSV.generate(@options) do |csv|
      csv << User.column_names
      @users.each do |user|
        csv << user.attributes.values_at(*User.column_names)
      end
    end
  end
end
