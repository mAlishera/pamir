class SearchController < ApplicationController

  def new
    @search = Search.new
  end

  def show
    @search = Search.new(params[:search])
    @users = @search.perform
  end
end
