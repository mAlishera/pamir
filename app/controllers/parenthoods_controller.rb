class ParenthoodsController < ApplicationController

  def create
    @parenthood = current_user.parenthoods.build(child_id: params[:child_id])
    if @parenthood.save
      flash[:notice] = "Added"
    else
      flash[:notice] = "Unable to add"
    end
  end
end
