class SpousesController < ApplicationController

  before_action :find_user
  before_action :spouse, only: [:destroy, :edit, :update]

  def new
    @spouse = User.new
  end

  def create
    @spouse = User.new(spouse_params)
    authorize @spouse
    if @spouse.save
      flash[:success] = "Your profile was updated."
      redirect_to @user
    else
      flash[:error] = @spouse.errors.full_messages
      render :edit
    end
  end

  def edit
  end

  def update
  end

  def destroy
    authorize @spouse
    @spouse.spouses.delete(@user)
    @user.spouses.delete(@spouse)    
    flash[:success] = "Your profile was updated."
    redirect_to @user
  end

  private

  def spouse_params
    params[:spouse][:role_ids] ||= []
    params[:spouse][:language_ids] ||= []
    params.require(:spouse).permit(:email, :middle_name, :first_name, :last_name, :phone, :gender, :grade, :university,
                                   :profession, :year_of_grace, :other_lang, :birthday, :education, :user_helps_gamoat,
                                   :bp_country, :bp_address, :bp_gamoat, :lp_country, :lp_address, :lp_district_metro,
                                   :passport_date_of_issue, :passport_number, :year_of_marriage, :year_of_graduation,
                                   :current_occupation, :income, :nationality, :state_of_living, :drivers_license, :reservist,
                                   :state_of_occupation, :state_of_separation, :marital_status, :international_passport,
                                   :other_nationality, :license_category, {role_ids: []}, {language_ids: []})
  end

  def spouse
    @spouse = @user.spouses.first
  end

  def find_user
    @user = User.find(params[:user_id])
  end
end
