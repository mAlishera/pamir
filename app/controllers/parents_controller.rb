class ParentsController < ApplicationController

  before_action :find_child

  def new
    @parent = User.new
  end

  def create
    @parent = User.new(parent_params)
    authorize @parent
    if @parent.save
      flash[:success] = "Your profile was updated."
      redirect_to @child
    else
      flash[:error] = @parent.errors.full_messages
      render :edit
    end
  end

  def edit
  end

  def update
  end

  def destroy
    @parent = User.find(params[:id])
    authorize @parent
    @parent.children.delete(@child)
    flash[:success] = "Your profile was updated."
    redirect_to @child
  end

  private

  def parent_params
    params.require(:parent).permit(:email, :middle_name, :first_name, :last_name, :phone, :gender, :language,
                                   :grade, :langlevel, :university, :profession, :year_of_grace, :other_lang,
                                   :birthday, :education)
  end

  def find_child
    @child = User.find(params[:user_id])
  end
end
