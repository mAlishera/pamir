class UsersExportsController < ApplicationController

  def show
    @users = User.order(:last_name).page params[:page]
    raise Pundit::NotAuthorizedError unless UsersExportPolicy.new(current_user, @users).show?
    respond_to do |format|
      format.csv { send_data UsersCsvExport.new(User.all).perform }
      format.xls { send_data UsersXlsExport.new(User.all).perform }
      format.xlsx { send_data UsersXlsxExport.new(User.all).perform, filename: "export.xlsx" }
    end
  end
end
