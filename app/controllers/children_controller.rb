class ChildrenController < ApplicationController

  before_action :find_parent
  before_action :find_child, only: [:destroy, :edit, :update]

  def index
    @children = @parent.children
  end

  def new
    @child = @parent.children.build
  end

  def create
    @child = @parent.children.build(child_params)
    authorize @child
    if @child.save
      @parent.children << @child
      flash[:success] = "Your profile was updated."
      redirect_to @parent
    else
      flash[:error] = @child.errors.full_messages
      render :edit
    end
  end

  def edit
  end

  def update
  end

  def destroy
    authorize @child
    @parent.children.delete(@child)
    flash[:success] = "Your profile was updated."
    redirect_to @parent
  end

  private

  def child_params
    params[:child][:role_ids] ||= []
    params[:child][:language_ids] ||= []
    params.require(:child).permit(:email, :middle_name, :first_name, :last_name, :phone, :gender, :grade, :university,
                                  :profession, :year_of_grace, :other_lang, :birthday, :education, :user_helps_gamoat,
                                  :bp_country, :bp_address, :bp_gamoat, :lp_country, :lp_address, :lp_district_metro,
                                  :passport_date_of_issue, :passport_number, :year_of_marriage, :year_of_graduation,
                                  :current_occupation, :income, :nationality, :state_of_living, :drivers_license, :reservist,
                                  :state_of_occupation, :state_of_separation, :marital_status, :international_passport,
                                  :other_nationality, :license_category, {role_ids: []}, {language_ids: []})
  end

  def find_child
    @child = User.find(params[:id])
  end

  def find_parent
    @parent = User.find(params[:user_id])
  end
end
