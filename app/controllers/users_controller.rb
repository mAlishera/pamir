require 'axlsx'

class UsersController < ApplicationController

  before_action :find_user, only: [:destroy, :show, :edit, :update, :new_parent, :create_parent]

  def index
    @users = User.order(:last_name).page params[:page]
    respond_to do |format|
      format.html
      format.json { render json: @users}
    end
  end

  def show
  end

  def new
    @user = User.new
    @user.languages.new
    @user.proficiencies.new
    @user.children.new
    @user.spouses.new
  end

  def create
    @user = User.new(user_params)
    authorize @user
    if @user.save
      flash[:success] = "Профайл успешно создан."
      redirect_to @user
    else
      flash[:error] = @user.errors.full_messages
      redirect_to new_user_path
    end
  end

  def update
    authorize @user
    if @user.update(user_params)
      params[:user][:language_ids].each do |a, b|
        @user.set_language_proficiency(b[:lang_id], b[:level])
      end
      if spouse = User.where(id: params[:select_spouse]).first
        @user.add_spouse(spouse)
      end
      if parent = User.where(id: params[:select_parent]).first
        @user.add_parent(parent)
      end
      if child = User.where(id: params[:select_child]).first
        @user.add_child(child)
      end
      flash[:success] = "Профайл был изменен."
      redirect_to @user
    else
      flash[:error] = @user.errors.full_messages
      render :edit
    end
  end

  def edit
    @user_options = User.all.collect { |u| [u.fullname, u.id] }
    @user.languages.new
    @user.proficiencies.new
    @user.children.new
    @user.spouses.new
  end

  def destroy
    authorize @user
    @user.delete
    respond_to do |format|
      format.html { redirect_to users_path }
      format.js
    end
  end

  private

  def user_params
    params[:user][:role_ids] ||= []
    params[:user][:language_ids] ||= []
    params.require(:user).permit(:email, :middle_name, :first_name, :last_name, :phone, :gender_cd, :grade,
                                 :university, :language_id, :profession, :year_of_grace, :other_lang,
                                 :birthday, :education, :user_helps_gamoat, :bp_country, :bp_address, :bp_gamoat,
                                 :lp_country, :lp_address, :lp_district_metro, :year_of_marriage, :year_of_graduation, :current_occupation,
                                 :income, :nationality, :state_of_living, :state_of_occupation, :state_of_separation, :marital_status,
                                 :other_nationality, :russian, :tajik, {role_ids: []}, {language_ids: []},
                                 languages_attributes: [:id, :name, :_destroy], proficiencies_attributes: [:language_id,
                                 :level, :_destroy], children_attributes: [:id, :email, :first_name, :last_name,
                                 :middle_name, :gender_cd, :current_occupation, :state_of_separation, :birthday,
                                 :_destroy], spouses_attributes: [:id, :email, :first_name, :last_name, :middle_name,
                                 :gender_cd, :current_occupation, :year_of_marriage, :state_of_separation, :birthday,
                                 :_destroy])
  end

  def find_user
    @user = User.find(params[:id]||params[:user_id])
  end
end
