class UsersImportsController < ApplicationController

  def create
    raise Pundit::NotAuthorizedError unless UsersImportPolicy.new(current_user, @file).create?
    UsersImport.new(params[:file]).perform
    redirect_to root_url, notice: "Users imported."
  rescue UsersImport::InvalidFileFormat => exception
    redirect_to root_url, alert: exception.message
  end
end
