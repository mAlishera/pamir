class UserPolicy < ApplicationPolicy

  def index?
    true
  end

  def show?
    true
  end

  def create?
    user.has_any_role?("operator", "moderator", "admin")
  end

  def update?
    user.has_any_role?("moderator", "admin")
  end

  def destroy?
    user.has_any_role?("admin")
  end

  def add_role?(name)
    user.has_any_role?("admin")
  end

  def delete_role?(name)
    user.has_any_role?("admin")
  end

  def set_default_role?
    true
  end
end
