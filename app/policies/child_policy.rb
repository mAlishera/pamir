class ChildPolicy < ApplicationPolicy

  def create?
    user.has_any_role?("operator", "moderator", "admin")
  end

  def update?
    user.has_any_role?("moderator", "admin")
  end

  def destroy?
    user.has_any_role?("admin")
  end
end