class UsersExportPolicy < ApplicationPolicy

  def show?
    user.has_any_role?("admin")
  end
end