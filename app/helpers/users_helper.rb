module UsersHelper
  def show_static_field(label, data)
    html = <<-HTML
    <div class="form-group">
      <label class="col-sm-4 control-label">#{label}</label>
      <div class="col-sm-8">
        <p class="form-control-static">#{data}</p>
      </div>
    </div>
    HTML
    html.html_safe
  end
end
