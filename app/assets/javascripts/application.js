// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require select2
//= require maskedinput
//= require jquery-ui/datepicker
//= require jquery_nested_form
//= require_tree .

jQuery(function($){ 
  $("#user_phone").mask("+7 (999) 999-99-99");
  $("select").select2();
  $("#e1").select2({ placeholder: 'найти пользователя', width: '200px' });
  $("#e2").select2({ placeholder: 'найти пользователя', width: '200px' });
  $("#e3").select2({ placeholder: 'найти пользователя', width: '200px' });
  $('.dropdown-toggle').dropdown();
  $('.datepicker').datepicker()
});
