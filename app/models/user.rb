class User < ActiveRecord::Base

  acts_as_xlsx

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable

  validates :email, uniqueness: true

  has_and_belongs_to_many :roles

  has_many :parenthoods
  has_many :children, through: :parenthoods

  has_many :marriages
  has_many :spouses, through: :marriages

  has_many :proficiencies
  has_many :languages, through: :proficiencies

  as_enum :gender, female: 1, male: 0
  as_enum :education, primary: 0, middle: 1, high: 2, student: 3
  as_enum :grade, none: 0, bachelor: 1, specialist: 2, master: 3, dr: 4
  as_enum :income, less_500: 0, less_1000: 1, more_1000: 2
  as_enum :state_of_living, temporarily: 0, permanently: 1
  as_enum :state_of_occupation, free: 0, busy: 1
  as_enum :marital_status, single: 0, married: 1, divorced: 2
  as_enum :state_of_separation, together: 0, apart: 1

  accepts_nested_attributes_for :proficiencies, allow_destroy: true
  accepts_nested_attributes_for :children, allow_destroy: true
  accepts_nested_attributes_for :spouses, allow_destroy: true

  after_create :set_default_role

  paginates_per 15

  def password_required?
    false
  end

  def email_required?
    false
  end

  def fullname
    [first_name, last_name].join(" ")
  end

  def add_role(name)
    unless self.roles.find_by_name(name)
      self.roles << Role.find_by_name(name)
    end
  end

  def delete_role(name)
    roles.delete(Role.find_by_name(name))
  end

  def has_any_role?(*args)
    !(self.roles.map(&:name)&args).empty?
  end

  def has_any_of_roles?
    !Role.where(id: self.role_ids).empty?
  end

  def set_default_role
    self.add_role("user")
  end

  def self.admins
    self.joins(:roles).where(roles: {name: "admin"})
  end

  def set_language_proficiency(language_id, level)
    proficiency = proficiencies.where(language_id: language_id, user_id: self.id).first_or_initialize
    proficiency.update_attribute(:level, level)
  end

  def proficiency_for(language)
    Proficiency.find_by(user: self, language: language)
  end

  def parents
    parent_ids = Parenthood.where(child_id: id).pluck(:user_id)
    User.find(parent_ids)
  end

  def add_spouse(user)
    self.spouse = user
    user.spouse = self
  end

  def add_child(user)
    self.children << user
  end

  def add_parent(user)
    user.children << self
  end
end
