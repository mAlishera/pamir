class SearchFormBuilder < ActionView::Helpers::FormBuilder
  delegate :content_tag, :submit_tag, :button_tag, :options_for_select, to: :@template

  def first_name
    t_field("first_name", "Имя", "введите имя")
  end

  def last_name
    t_field("last_name", "Фамилия", "введите фамилию")
  end

  def middle_name
    t_field("middle_name", "Отчество", "введите отчество")
  end

  def university
    t_field("university", "ВУЗ", "введите учебное заведение")
  end

  def email
    t_field("email", "email", "введите email")
  end

  def role
    content_tag(:div, class: 'form-group form-inline col-md-12') do
      label(:role, "права доступа", class: "col-md-3 text-right") +
        collection_select(:role, Role.all, :id, :name, prompt: "выбрать")
    end
  end

  def language
    content_tag(:div, class: 'form-group form-inline col-md-12') do
      label(:language, "язык", class: "col-md-3 text-right") +
        collection_select(:language, Language.all, :id, :name, prompt: "выбрать")
    end
  end

  def spouse
    content_tag(:div, class: 'form-group col-md-9') do
      content_tag(:div, class: ' form-inline radio col-md-2') do
        label(:spouse, "да") +
        radio_button(:spouse, true)
      end +
      content_tag(:div, class: 'form-inline radio col-md-2 radio-no') do
        label(:spouse, "нет") +
        radio_button(:spouse, false)
      end
    end
  end

  def children
    content_tag(:div, class: 'form-group col-md-9') do
      content_tag(:div, class: 'radio inline col-md-2') do
        label(:children, "да") +
        radio_button(:children, true) 
      end +
      content_tag(:div, class: 'radio inline col-md-2 radio-no') do
        label(:children, "нет") +
        radio_button(:children, false)
      end
    end
  end

  def gender
    content_tag(:div, class: 'form-group col-md-9') do
      content_tag(:div, class: ' form-inline radio col-md-2') do
        label(:gender, "муж") +
        radio_button(:gender, "male")
      end +
      content_tag(:div, class: 'form-inline radio col-md-2 radio-no') do
        label(:gender, "жен") +
        radio_button(:gender, "female")
      end
    end
  end

  private

  def t_field(name, label, placeholder)
    content_tag(:div, class: 'form-group form-inline col-md-12') do
      label(name.to_sym, label, class: "col-md-3 text-right") +
        text_field(name.to_sym, class: "form-control col-md-9 xt-input", placeholder: placeholder)
    end
  end
end
