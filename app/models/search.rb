class Search

  include ActiveModel::Model

  attr_accessor :first_name, :email, :last_name, :middle_name, :university, :role, :language, :spouse, :children,
                :gender, :nationality

  def initialize(params = {})
    @first_name = params[:first_name]
    @last_name = params[:last_name]
    @middle_name = params[:middle_name]
    @email = params[:email]
    @university = params[:university]
    @role = params[:role]
    @language = params[:language]
    @spouse = params[:spouse]
    @children = params[:children]
    @gender = params[:gender]
    @nationality = params[:nationality]  
  end

  def perform
    scope = UsersIndex::User.query(match_all: { } )
    fields = {languages: language, roles: role, spouse: spouse, email: email, first_name: first_name, university: university,
              middle_name: middle_name, last_name: last_name, children: children, gender: gender, nationality: nationality}
    fields.each{ |k, v| scope = scope.query(match: { k => v } ) unless v.blank? }
    scope
  end

  def self.model_name
    ActiveModel::Name.new(self, nil, "Search")
  end

  def persisted?
    true
  end
end
