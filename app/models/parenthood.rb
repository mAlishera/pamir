class Parenthood < ActiveRecord::Base
  belongs_to :user
  belongs_to :child, class_name: 'User'
end
