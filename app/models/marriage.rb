class Marriage < ActiveRecord::Base
  belongs_to :user
  belongs_to :spouse, class_name: 'User'

  after_create :add_spouse

  def add_spouse
    unless Marriage.where(user: spouse, spouse: user).exists?
      Marriage.new do |obj|
        obj.user = spouse
        obj.spouse = user
      end.save!
    end
  end
end
