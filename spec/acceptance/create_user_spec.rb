require 'spec_helper'

feature 'create user' do
  let!(:role) {create :role}
  let!(:admin) {create :user, :with_role_admin}
  let!(:user) {create :user}

  scenario 'admin create user' do
    sign_in(admin)
    visit root_path
    click_on 'New user'
    fill_in 'first name', with: 'my title'
    fill_in 'last name', with: 'my title'
    fill_in 'middle name', with: 'my title'
    fill_in 'phone', with: 4956788878
    fill_in 'email', with: 'sagha@ajlf.ru'
    select('male', from: 'gender')
    choose("user_education_middle")
    fill_in 'university', with: 'my title'
    select('none', from: 'grade')
    fill_in 'profession', with: 'my title'
    check("user_role_ids_#{Role.find_by_name("admin").id}")
    click_on 'сохранить'
    expect(page).to have_content('Профайл успешно создан.')
  end

  scenario 'user cannot create user' do
    sign_in(user)
    visit root_path
    click_on 'New user'
    fill_in 'first name', with: 'my title'
    fill_in 'last name', with: 'my title'
    fill_in 'middle name', with: 'my title'
    fill_in 'phone', with: 4956788878
    fill_in 'email', with: 'sagha@ajlf.ru'
    fill_in 'university', with: 'my title'
    fill_in 'profession', with: 'my title'
    click_on 'сохранить'
    expect(page).to have_content("У вас недостаточно прав")
  end
end
