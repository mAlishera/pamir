FactoryGirl.define do
  factory :role do
    name 'user'
  end

  factory :admin_role, class: Role do
    name 'admin'
  end

  factory :moderator_role, class: Role do
    name 'moderator'
  end
  
  factory :operator_role, class: Role do
    name 'operator'
  end
end