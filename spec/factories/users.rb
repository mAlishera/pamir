# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  sequence :email do |n|
    "emaqqq#{n}@factory.com"
  end

  factory :user do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    email
    password "foobarrr"
    password_confirmation { |u| u.password }

    trait :with_role_admin do
      after :create do |object|
        object.roles.create name: "admin"
      end
    end
    trait :with_role_moderator do
      after :create do |object|
        object.roles.create name: "moderator"
      end
    end
    trait :with_role_operator do
      after :create do |object|
        object.roles.create name: "operator"
      end
    end
  end

  factory :spouse, class: User do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    email
    password "foobarrr"
    password_confirmation { |u| u.password }

    trait :with_user do
      after :create do |object|
        user = create(:user)
        object.spouse = user
        user.spouse = object
      end
    end
  end

  factory :child, class: User do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    email
    password "foobarrr"
    password_confirmation { |u| u.password }

    trait :with_parent do
      after :create do |object|
        parent = create(:user)
        parent.children<<object
      end
    end
  end

  factory :parent, class: User do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    email
    password "foobarrr"
    password_confirmation { |u| u.password }

    trait :with_child do
      after :create do |object|
        child = create(:user)
        object.children<<child
      end
    end
  end
end
