require 'spec_helper'

describe UsersImport do

  let(:file) {File.open("#{Rails.root}/spec/support/import_test.csv")}

  it 'imports users from file' do
    expect {
      described_class.new(file).perform
    }.to change(User, :count).by(1)
  end
end