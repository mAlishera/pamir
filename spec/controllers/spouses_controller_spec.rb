require 'spec_helper'

describe SpousesController do
  let!(:role) {create :role}
  let!(:admin_role) {create :admin_role}
  let!(:moderator_role) {create :moderator_role}
  let!(:operator_role) {create :operator_role}
  let!(:spouse) {create :spouse, :with_user}
  let!(:user) {spouse.spouse}

  context 'admin role' do
    let!(:admin) {create :user, :with_role_admin}
    before { sign_in admin }

    describe '#destroy' do
      it "admin can destroy user" do
        expect {
          delete :destroy, user_id: user, id: spouse
        }.to change(User, :count).by(-1)
      end
    end

    describe '#create' do
      it "admin can create a user" do
        expect {
          post :create, user_id: user, spouse: {"first_name"=>"Name",
                                                "last_name"=>"",
                                                "middle_name"=>"",
                                                "phone"=>"",
                                                "email"=>"myemail@fghj.ru",
                                                "birthday(3i)"=>"9",
                                                "birthday(2i)"=>"9",
                                                "birthday(1i)"=>"2014",
                                                "gender"=>"female",
                                                "university"=>"",
                                                "grade"=>"none",
                                                "profession"=>""}
        }.to change(User, :count).by(1)
      end
    end

    describe '#update' do
      it "admin can change user info" do
        new_email = 'jackass@gmail.com'

        patch :update, user_id: user, spouse: {email: new_email}
        spouse.reload
        expect(spouse.email).to eq(new_email)
      end
    end
  end

  context 'moderator role' do
    let!(:moderator) {create :user, :with_role_moderator}
    before { sign_in moderator }

    describe '#destroy' do
      it "moderator can't destroy user" do
        expect {
          delete :destroy, user_id: user, id: spouse
        }.to change(User, :count).by(0)
      end
    end

    describe '#create' do
      it "moderator can create a user" do
        expect {
          post :create, user_id: user, spouse: {"first_name"=>"Name",
                                                "last_name"=>"",
                                                "middle_name"=>"",
                                                "phone"=>"",
                                                "email"=>"myemail@fghj.ru",
                                                "birthday(3i)"=>"9",
                                                "birthday(2i)"=>"9",
                                                "birthday(1i)"=>"2014",
                                                "gender"=>"female",
                                                "university"=>"",
                                                "grade"=>"none",
                                                "profession"=>""}
        }.to change(User, :count).by(1)
      end
    end

    describe '#update' do
      it "moderator can change user info" do
        new_email = 'jackass@gmail.com'
        patch :update, user_id: user, spouse: {email: new_email}
        spouse.reload
        expect(spouse.email).to eq(new_email)
      end
    end
  end

  context 'operator role' do
    let!(:operator) {create :user, :with_role_operator}
    before { sign_in operator }

    describe '#destroy' do
      it "operator can't destroy user" do
        expect {
          delete :destroy, user_id: user, id: spouse
        }.to change(User, :count).by(0)
      end
    end

    describe '#create' do
      it "operator can create a user" do
        expect {
          post :create, user_id: user, spouse: {"first_name"=>"Name",
                                                "last_name"=>"",
                                                "middle_name"=>"",
                                                "phone"=>"",
                                                "email"=>"myemail@fghj.ru",
                                                "birthday(3i)"=>"9",
                                                "birthday(2i)"=>"9",
                                                "birthday(1i)"=>"2014",
                                                "gender"=>"female",
                                                "university"=>"",
                                                "grade"=>"none",
                                                "profession"=>""}
        }.to change(User, :count).by(1)
      end
    end

    describe '#update' do
      it "operator can't change user info" do
        new_email = 'jackass@gmail.com'
        patch :update, user_id: user, spouse: {email: new_email}
        spouse.reload
        expect(spouse.email).to_not eq(new_email)
      end
    end
  end

  context 'user role' do
    before { sign_in user }

    describe '#destroy' do
      it "user can't destroy questions" do
        expect {
          delete :destroy, user_id: user, id: spouse
        }.to change(User, :count).by(0)
      end
    end

    describe '#create' do
      it "user can't create a user" do
        expect {
          post :create, user_id: user, spouse: {"first_name"=>"Name",
                                                "last_name"=>"",
                                                "middle_name"=>"",
                                                "phone"=>"",
                                                "email"=>"myemail@fghj.ru",
                                                "birthday(3i)"=>"9",
                                                "birthday(2i)"=>"9",
                                                "birthday(1i)"=>"2014",
                                                "gender"=>"female",
                                                "university"=>"",
                                                "grade"=>"none",
                                                "profession"=>""}
        }.to change(User, :count).by(0)
      end
    end

    describe '#update' do
      it "user can't change user info" do
        new_email = 'jackass@gmail.com'
        patch :update, user_id: user, spouse: {email: new_email}
        spouse.reload
        expect(spouse.email).to_not eq(new_email)
      end
    end
  end
end
