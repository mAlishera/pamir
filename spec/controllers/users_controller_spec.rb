require 'spec_helper'

describe UsersController do

  let!(:role) {create :role}
  let!(:admin_role) {create :admin_role}
  let!(:moderator_role) {create :moderator_role}
  let!(:operator_role) {create :operator_role}
  let!(:user) {create :user}

  context 'admin role' do
    let!(:admin) {create :user, :with_role_admin}
    before { sign_in admin }

    describe '#index' do
      let(:users) { FactoryGirl.create_list(:user, 4) }
      before { get :index }

      it 'populates users' do
        expect(assigns(:users)).to match_array(users<<user<<admin)
      end

      it "renders the index template" do
        expect(response).to render_template :index
      end
    end

    describe '#destroy' do
      it 'redirect to users' do
        delete :destroy, id: user
        expect(response).to redirect_to users_path
      end

      it "admin can destroy user" do
        expect {
          delete :destroy, id: user
        }.to change(User, :count).by(-1)
      end
    end

    describe '#show' do
      it 'should assign user to @user' do
        get :show, id: user
        expect(assigns(:user)).to eq user
      end

      it 'should render @user profile' do
        get :show, id: user
        expect(response).to render_template :show
      end
    end

    describe '#new' do
      it 'assigns a new User to @user' do
        get :new
        expect(assigns(:user)).to be_a_new(User)
      end

      it 'should render the new user template' do
        get :new
        expect(response).to render_template :new
      end
    end

    describe '#create' do
      it "admin can create a user" do
        expect {
          post :create, user: {"first_name"=>"Name",
                               "last_name"=>"",
                               "middle_name"=>"",
                               "phone"=>"",
                               "email"=>"myemail@fghj.ru",
                               "birthday(3i)"=>"9",
                               "birthday(2i)"=>"9",
                               "birthday(1i)"=>"2014",
                               "gender"=>"female",
                               "university"=>"",
                               "grade"=>"none",
                               "profession"=>""}
        }.to change(User, :count).by(1)
      end
    end

    describe '#edit' do
      it "should render the edit template" do
        get :edit, id: user
        expect(response).to render_template :edit
      end
    end

    describe '#update' do
      it "admin can change user info" do
        new_email = 'jackass@gmail.com'
        patch :update, id: user, user: {email: new_email}
        user.reload
        expect(user.email).to eq(new_email)
      end
    end
  end

  context 'moderator role' do
    let!(:moderator) {create :user, :with_role_moderator}
    before { sign_in moderator }

    describe '#destroy' do
      it "moderator can't destroy user" do
        expect {
          delete :destroy, id: user
        }.to change(User, :count).by(0)
      end
    end

    describe '#create' do
      it "moderator can create a user" do
        expect {
          post :create, user: {"first_name"=>"Name",
                               "last_name"=>"",
                               "middle_name"=>"",
                               "phone"=>"",
                               "email"=>"myemail@fghj.ru",
                               "birthday(3i)"=>"9",
                               "birthday(2i)"=>"9",
                               "birthday(1i)"=>"2014",
                               "gender"=>"female",
                               "university"=>"",
                               "grade"=>"none",
                               "profession"=>""}
        }.to change(User, :count).by(1)
      end
    end

    describe '#update' do
      it "moderator can change user info" do
        new_email = 'jackass@gmail.com'
        patch :update, id: user, user: {email: new_email}
        user.reload
        expect(user.email).to eq(new_email)
      end
    end
  end

  context 'operator role' do
    let!(:operator) {create :user, :with_role_operator}
    before { sign_in operator }

    describe '#destroy' do
      it "operator can't destroy user" do
        expect {
          delete :destroy, id: user
        }.to change(User, :count).by(0)
      end
    end

    describe '#create' do
      it "operator can create a user" do
        expect {
          post :create, user: {"first_name"=>"Name",
                               "last_name"=>"",
                               "middle_name"=>"",
                               "phone"=>"",
                               "email"=>"myemail@fghj.ru",
                               "birthday(3i)"=>"9",
                               "birthday(2i)"=>"9",
                               "birthday(1i)"=>"2014",
                               "gender"=>"female",
                               "university"=>"",
                               "grade"=>"none",
                               "profession"=>""}
        }.to change(User, :count).by(1)
      end
    end

    describe '#update' do
      it "operator can't change user info" do
        new_email = 'jackass@gmail.com'
        patch :update, id: user, user: {email: new_email}
        user.reload
        expect(user.email).to_not eq(new_email)
      end
    end
  end

  context 'user role' do
    before { sign_in user }

    describe '#destroy' do
      it "user can't destroy questions" do
        expect {
          delete :destroy, id: user
        }.to change(User, :count).by(0)
      end
    end

    describe '#create' do
      it "user can't create a user" do
        expect {
          post :create, user: {"first_name"=>"Name",
                               "last_name"=>"",
                               "middle_name"=>"",
                               "phone"=>"",
                               "email"=>"myemail@fghj.ru",
                               "birthday(3i)"=>"9",
                               "birthday(2i)"=>"9",
                               "birthday(1i)"=>"2014",
                               "gender"=>"female",
                               "university"=>"",
                               "grade"=>"none",
                               "profession"=>""}
        }.to change(User, :count).by(0)
      end
    end

    describe '#update' do
      it "user can't change user info" do
        new_email = 'jackass@gmail.com'
        patch :update, id: user, user: {email: new_email}
        user.reload
        expect(user.email).to_not eq(new_email)
      end
    end
  end
end
