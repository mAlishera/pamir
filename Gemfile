source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.0.rc1'

gem 'pg'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.1'
gem 'bootstrap-sass', '~> 3.1.1'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer',  platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'

gem 'devise'
gem 'simple_enum'
gem 'kaminari'
gem 'bootstrap-kaminari-views'
gem 'select2-rails'
gem 'maskedinput-rails'
gem 'faker'
gem 'pundit'
gem 'axlsx', '~> 2.0.1'
gem 'axlsx_rails'
gem 'acts_as_xlsx'
gem 'roo'
gem 'chewy'
gem 'jquery-ui-rails'
gem 'nested_form'

group :development, :test do
  gem 'rspec-rails'

  gem 'thin'
  gem 'quiet_assets'

  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'xray-rails', github: 'shemerey/xray-rails', branch: 'rails_4.1_bugfix'
  gem 'pry'
  gem 'pry-stack_explorer'
  gem 'pry-debugger'
  gem 'spring'
  gem 'capybara'
  gem 'launchy'
  gem 'capistrano', '~> 3.1.0'
  gem 'capistrano-rails'
  gem 'capistrano-rvm'
  gem 'capistrano-bundler', '~> 1.1.2'
  gem 'capistrano3-unicorn'

  gem 'shoulda-matchers'
  gem 'database_cleaner'
  gem 'factory_girl_rails', '~> 4.0'
  gem 'pry-rails'
  gem 'json_spec'
end

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.1.2'

# Use unicorn as the app server
gem 'unicorn'
