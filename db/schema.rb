# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150304155307) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "languages", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "langlevel_cd"
  end

  create_table "languages_users", id: false, force: true do |t|
    t.integer "language_id"
    t.integer "user_id"
  end

  create_table "marriages", force: true do |t|
    t.integer  "user_id"
    t.integer  "spouse_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "parenthoods", force: true do |t|
    t.integer  "user_id"
    t.integer  "child_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "proficiencies", force: true do |t|
    t.integer  "user_id"
    t.integer  "language_id"
    t.integer  "level"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles_users", id: false, force: true do |t|
    t.integer "role_id"
    t.integer "user_id"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name"
    t.string   "last_name"
    t.date     "birthday"
    t.integer  "gender_cd"
    t.integer  "education_cd"
    t.integer  "grade_cd"
    t.integer  "langlevel_cd"
    t.string   "university"
    t.string   "profession"
    t.string   "other_lang"
    t.string   "middle_name"
    t.string   "phone"
    t.string   "user_helps_gamoat"
    t.string   "bp_country"
    t.string   "bp_address"
    t.string   "bp_gamoat"
    t.string   "lp_country"
    t.string   "lp_address"
    t.string   "lp_district_metro"
    t.date     "year_of_marriage"
    t.date     "year_of_graduation"
    t.string   "current_occupation"
    t.integer  "income_cd"
    t.integer  "nationality_cd"
    t.integer  "state_of_living_cd"
    t.integer  "state_of_occupation_cd"
    t.integer  "state_of_separation_cd"
    t.integer  "marital_status_cd"
    t.string   "other_nationality"
    t.boolean  "russian",                default: false
    t.boolean  "tajik",                  default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
