# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.delete_all
Role.delete_all
Language.delete_all

["admin", "moderator", "operator", "user"].each do |name|
  Role.create!(name: name)
end

["EN", "GE", "FR", "AR", "ZH", "FA"].each do |name|
  Language.create!(name: name)
end

(1..15).each do |index|
  user = User.new(first_name: Faker::Name.first_name, middle_name: Faker::Name.first_name, last_name: Faker::Name.last_name, phone: Faker::PhoneNumber.phone_number, gender: 1, birthday: DateTime.strptime("09/01/2009", "%m/%d/%Y"), email: Faker::Internet.email, password: "12345678")
  user.save!
end

User.first.children<<User.second
User.third.children<<User.fourth<<User.fifth
User.fifth.spouses<< User.fourth
User.fourth.spouses << User.fifth
User.last.spouses << User.first
User.first.spouses << User.last
  
kate = User.new(first_name: "Ekaterina", last_name: "SH", email: "rooh@mail.ru", password: "12345678")
kate.save!
kate.add_role("admin")

operator = User.new(first_name: "operator", last_name: "SH_last_name", email: "operator@mail.ru", password: "12345678")
operator.save!
operator.add_role("operator")

user = User.new(first_name: "user", last_name: "SH_last_name", email: "user@mail.ru", password: "12345678")
user.save!
user.add_role("user")

moderator = User.new(first_name: "moderator", last_name: "SH_last_name", email: "moderator@mail.ru", password: "12345678")
moderator.save!
moderator.add_role("moderator")

puts "You can login with email: #{kate.email} password: #{kate.password}"
