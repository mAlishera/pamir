class AddProficiencyTable < ActiveRecord::Migration
  def change
    create_table :proficiencies do |t|
      t.belongs_to :user
      t.belongs_to :language
      t.integer :level
      t.timestamps
    end
  end
end
