class RemoveColumnFromUser < ActiveRecord::Migration
  def change
    remove_column :users, :international_passport_cd
    remove_column :users, :license_category
    remove_column :users, :reservist_cd
    remove_column :users, :drivers_license_cd
    remove_column :users, :passport_number
    remove_column :users, :passport_date_of_issue
  end
end
