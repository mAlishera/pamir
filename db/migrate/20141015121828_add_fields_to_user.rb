class AddFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :user_helps_gamoat, :string
    add_column :users, :bp_country, :string
    add_column :users, :bp_address, :string
    add_column :users, :bp_gamoat, :string
    add_column :users, :lp_country, :string
    add_column :users, :lp_address, :string
    add_column :users, :lp_district_metro, :string
    add_column :users, :passport_number, :string
    add_column :users, :passport_date_of_issue, :date
    add_column :users, :year_of_marriage, :date
    add_column :users, :year_of_graduation, :date
    add_column :users, :current_occupation, :string
  end
end
