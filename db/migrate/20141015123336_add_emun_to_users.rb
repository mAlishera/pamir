class AddEmunToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :income_cd, :integer
    add_column :users, :nationality_cd, :integer
    add_column :users, :state_of_living_cd, :integer
    add_column :users, :drivers_license_cd, :integer
    add_column :users, :state_of_occupation_cd, :integer
    add_column :users, :reservist_cd, :integer
  end

  def self.down
    remove_column :users, :income_cd
    remove_column :users, :nationality_cd
    remove_column :users, :state_of_living_cd
    remove_column :users, :drivers_license_cd
    remove_column :users, :state_of_occupation_cd
    remove_column :users, :reservist_cd
  end
end
