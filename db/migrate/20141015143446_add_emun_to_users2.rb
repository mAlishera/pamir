class AddEmunToUsers2 < ActiveRecord::Migration
  def self.up
    add_column :users, :state_of_separation_cd, :integer
    add_column :users, :marital_status_cd, :integer
    add_column :users, :international_passport_cd, :integer
  end

  def self.down
    remove_column :users, :state_of_separation_cd
    remove_column :users, :marital_status_cd
    remove_column :users, :international_passport_cd
  end
end
