class AddToUsers2 < ActiveRecord::Migration
  def change
  	add_column :users, :university, :string
  	add_column :users, :profession, :string
  	add_column :users, :year_of_grace, :date
  	add_column :users, :other_lang, :string
  end
end
