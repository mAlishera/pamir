class ChangeLanglevel < ActiveRecord::Migration
  def self.up
    add_column :languages, :langlevel_cd, :integer
  end

  def self.down
    remove_column :languages, :langlevel_cd
  end
end
