class RemoveEnumFromUser < ActiveRecord::Migration
  def change
    def self.up
      remove_column :users, :nationality_cd
    end

    def self.down
      add_column :users, :nationality_cd, :integer
    end
  end
end
