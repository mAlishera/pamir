class LanglevelCd < ActiveRecord::Migration
  def self.up
    add_column :users, :langlevel_cd, :integer
  end

  def self.down
    remove_column :users, :langlevel_cd
  end
end
