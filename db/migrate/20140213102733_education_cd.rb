class EducationCd < ActiveRecord::Migration
  def self.up
    add_column :users, :education_cd, :integer
  end

  def self.down
    remove_column :users, :education_cd
  end
end
