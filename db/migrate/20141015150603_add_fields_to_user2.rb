class AddFieldsToUser2 < ActiveRecord::Migration
  def change
    add_column :users, :other_nationality, :string
    add_column :users, :license_category, :string
  end
end
