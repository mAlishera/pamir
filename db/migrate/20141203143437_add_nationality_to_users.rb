class AddNationalityToUsers < ActiveRecord::Migration
  def change
    add_column :users, :russian, :boolean, default: false
    add_column :users, :tajik, :boolean, default: false  
  end
end
