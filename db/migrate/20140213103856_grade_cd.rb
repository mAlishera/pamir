class GradeCd < ActiveRecord::Migration
  def self.up
    add_column :users, :grade_cd, :integer
  end

  def self.down
    remove_column :users, :grade_cd
  end
end
