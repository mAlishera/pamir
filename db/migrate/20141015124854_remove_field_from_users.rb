class RemoveFieldFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :language_cd
    remove_column :users, :year_of_grace    
  end
end
