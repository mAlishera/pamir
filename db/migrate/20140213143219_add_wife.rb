class AddWife < ActiveRecord::Migration
  def change
    create_table :marriages do |t|
      t.integer :user_id
      t.integer :spouse_id

      t.timestamps
    end
  end
end
