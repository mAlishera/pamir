class LanguageCd < ActiveRecord::Migration
  def self.up
    add_column :users, :language_cd, :integer
  end

  def self.down
    remove_column :users, :language_cd
  end
end
